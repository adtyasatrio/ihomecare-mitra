// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  css: ['~/assets/css/main.css'],
  app: {
    head: {
      viewport: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, minimal-ui', 
    },
  },
  modules: [
    '@kevinmarrec/nuxt-pwa',
    '@nuxtjs/google-fonts'
  ],
  googleFonts: {
    families: {
      Manrope: [200, 300, 400, 500, 600, 700, 800],
    },
    prefetch: true,
    preconnect: true
  },
  pwa: {
    icon: {
      source: 'static/icon.png',
      fileName: 'icon.png',
    },
    meta: {
      name: 'iHomeCare',
      title: 'iHomeCare',
      author: 'Aditya Satrio W',
      mobileAppIOS: true,
      theme_color: '#ffffff',
    },
    manifest: {
      name: 'iHomeCare',
    }
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
})